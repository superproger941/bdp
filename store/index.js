import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    packages: []
  },
  getters: {
    allPackages (state) {
      return state.packages
    }
  },
  mutations: {
    SET_PACKAGES (state, packages) {
      state.packages = packages
    }
  },
  actions: {
    async getPackages ({ commit }, text) {
      const baseUrl = process.env.BASE_URL || 'https://registry.npmjs.org'
      const packages = await axios.get(`${baseUrl}/-/v1/search?text=/${text}`)

      commit('SET_PACKAGES', packages.data.objects)
    }
  }
})

export default store
